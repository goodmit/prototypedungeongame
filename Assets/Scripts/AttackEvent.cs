﻿using UnityEngine;

public class AttackEvent : MonoBehaviour {

    private IDamagable damager;

	// Use this for initialization
	void Start () {
        damager = gameObject.GetComponentInParent<IDamagable>();
	}

    /// <summary>
    /// Проводит атаку по цели
    /// </summary>
    public void Attack()
    {
        if(damager != null)
        {
            damager.DoDamage();
        }
    }
}
