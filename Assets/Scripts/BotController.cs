﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Реализует логику ботов
/// </summary>
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
public class BotController : MonoBehaviour, IDamagable, IInteractable {

    [Tooltip("Радиус обнаружения игрока")]
    [SerializeField]
    private float lookRadius;
    [Tooltip("Радиус атаки")]
    [SerializeField]
    private float attackRadius;
    [Tooltip("Пауза между атаками")]
    [SerializeField]
    private float attackRate = 1f;
    [Tooltip("Максимальное количество здоровья")]
    [SerializeField]
    private int maxHealth = 1;
    [Tooltip("Сила атаки (наносимый урон)")]
    [SerializeField]
    private int damageForce = 1;

    private int curHealth;
    private NavMeshAgent agent;
    private Animator anim;
    private bool walking;
    private bool isDead;
    private float attackCooldown = 0;
    private Transform targetPlayer;

	// Use this for initialization
	void Start () {
        isDead = false;
        curHealth = maxHealth;
        anim = GetComponentInChildren<Animator>();
        agent = GetComponentInChildren<NavMeshAgent>();
        targetPlayer = GameController.Instance.player.transform;
    }
	
	// Update is called once per frame
	void Update () {

        if (isDead) return;
        anim.SetBool("walking", walking);
        anim.SetBool("idling", !walking);

        float distance = Vector3.Distance(transform.position, targetPlayer.position);
        if(distance <= lookRadius)
        {
            MoveToAttack();
        }
        else
        {
            walking = false;
            agent.isStopped = true;
        }

    }

    // передвижение к цели с последующей атакой
    void MoveToAttack()
    {
        agent.destination = targetPlayer.position;

        if(!agent.pathPending && agent.remainingDistance > attackRadius)
        {
            agent.isStopped = false;
            walking = true;
        }
        else if (!agent.pathPending && agent.remainingDistance <= attackRadius)
        {
            
            anim.SetBool("fighting", false);
            transform.LookAt(targetPlayer);

            if(Time.time > attackCooldown)
            {
                attackCooldown = Time.time + attackRate;
                anim.SetBool("fighting", true);
            }

            agent.isStopped = true;
            walking = false;
        }
    }

    /// <summary>
    /// Наносим урон по цели
    /// </summary>
    public void DoDamage()
    {
        if (targetPlayer == null) return;
        targetPlayer.GetComponent<IInteractable>().Interact(damageForce);
    }

    /// <summary>
    /// Получение урона
    /// </summary>
    /// <param name="damage"></param>
    public void Interact(int damage)
    {
        curHealth -= damage;
        if(curHealth <= 0)
        {
            Destroy(gameObject);
        }
    }
    
    /*
    private void OnDrawGizmos()
    {
        UnityEditor.Handles.color = Color.yellow;
        UnityEditor.Handles.DrawWireArc(transform.position + new Vector3(0, 0.2f, 0), transform.up, transform.right, 360, lookRadius);

        UnityEditor.Handles.color = Color.red;
        UnityEditor.Handles.DrawWireArc(transform.position + new Vector3(0, 0.2f, 0), transform.up, transform.right, 360, attackRadius);
    }*/
}
