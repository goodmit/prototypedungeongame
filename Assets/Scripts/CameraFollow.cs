﻿using UnityEngine;

/// <summary>
/// Класс, отвечающий за плавное слежение камеры за объектом
/// </summary>
public class CameraFollow : MonoBehaviour
{
    [Tooltip("Скорость реагирования камеры на смещение цели")]
    [SerializeField]
    private float _lerpIndex;

    [Tooltip("Цель слежения камеры")]
    [SerializeField]
    private GameObject _target;

    private Vector3 _offset;
    
    // Start is called before the first frame update
    void Start()
    {
        _offset = _target.transform.position - transform.position;
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {
        if(!_target)
        {
            print("Camera has not a target!");
            return;
        }
        Follow();
    }

    public void SetTarget(GameObject target)
    {
        transform.position = new Vector3(target.transform.position.x + 1, transform.position.y, target.transform.position.z - 3);
        _target = target;
        _offset = _target.transform.position - transform.position;
    }

    #region Private
    private void Follow()
    {
        Vector3 targetPosition = _target.transform.position - _offset;
        transform.position = Vector3.Lerp(transform.position, targetPosition, _lerpIndex * Time.deltaTime);
    }
    #endregion
}
