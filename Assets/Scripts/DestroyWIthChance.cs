﻿using UnityEngine;

/// <summary>
/// Класс, который уничтожает объект с определенной вероятностью при его появлении на сцене
/// Удобен, когда надо при создании сцены отобразить рандомное количество объектов
/// </summary>
public class DestroyWIthChance : MonoBehaviour {

    [Range(0, 1)]
    [SerializeField]
    private float destroyChance = 0.5f;

    private void Start()
    {
        if (Random.value > destroyChance) Destroy(gameObject);
    }
}
