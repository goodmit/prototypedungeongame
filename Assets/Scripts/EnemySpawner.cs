﻿using UnityEngine;

/// <summary>
/// Генератор ботов
/// </summary>
public class EnemySpawner : MonoBehaviour {

    [SerializeField]
    private BotController[] bots;
    
    public void SpawnEnemy()
    {
        int botIndex = Random.Range(0, bots.Length);
        Instantiate(bots[botIndex], gameObject.transform.position, Quaternion.Euler(0, Random.Range(0, 360), 0), transform.parent.transform.parent);
    }
}
