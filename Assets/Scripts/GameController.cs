﻿using UnityEngine;

[RequireComponent(typeof(RoomSpawner))]
public class GameController : Singleton<GameController>
{
    public UIController UIManager;
    public PlayerController player;
    private RoomSpawner roomSpawner;

    private void Start()
    {
        roomSpawner = GetComponent<RoomSpawner>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }       
    }
    
    /* тоже бы сделать через Event */
    /// <summary>
    /// Обновляем список комнат
    /// </summary>
    /// <param name="playerRoom">комната, куда перешел игрок</param>
    public void UpdateRoomList(Room playerRoom)
    {
        StartCoroutine(roomSpawner.UpdateRooms(playerRoom));
    }

}
