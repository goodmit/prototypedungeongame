﻿using UnityEngine;

/// <summary>
/// Контроллер гейта (прохода ) комнаты
/// </summary>
public class Gate : MonoBehaviour {

    /// <summary>
    /// базовое направление гейта
    /// </summary>
    public enum Direction
    {
        NONE = -1,
        FORWARD,
        RIGHT,
        BACK,
        LEFT
    }

    /// <summary>
    /// состояние гейта
    /// залинкован (связан с другой комнатой) или нет
    /// </summary>
    public enum Connect
    {
        NONE,
        COLLAPSED,
        CONNECTED
    }

    [SerializeField]
    private Direction direction;
    [SerializeField]
    private Connect connect;

    private Room connectedRoom;

    private void Awake()
    {
        if (connect == Connect.NONE) connect = Connect.COLLAPSED;
        int rotateOffset = (int)(transform.parent.transform.localEulerAngles.y / 90);
        SwitchDirection(rotateOffset);
    }
	
    /// <summary>
    /// Обновить состояние гейта
    /// </summary>
    public void UpdateState()
    {
        connect = IsConnected() ? Connect.CONNECTED : Connect.COLLAPSED;
    }

    /// <summary>
    /// Привязка к комнате
    /// </summary>
    /// <param name="room"></param>
	public void SetLinkedRoom(Room room)
    {
        connectedRoom = room;
        connect = Connect.CONNECTED;
    }

    /// <summary>
    /// Проверяем, залинкован ли гейт
    /// </summary>
    /// <returns></returns>
    public bool IsConnected()
    {
        return connectedRoom != null;
    }

    /// <summary>
    /// Получаем направление гейта
    /// </summary>
    /// <returns></returns>
    public Direction GetDirection()
    {
        return direction;
    }

    /// <summary>
    /// Устанавливаем новое направление гейта
    /// </summary>
    /// <param name="value"></param>
    public void SetDirection(int value)
    {
        
        int dir = value % 4;
        direction = (Direction)dir;
    }

    /// <summary>
    /// Получаем комнату, в которой находится гейт
    /// </summary>
    /// <returns></returns>
    public Room GetParenRoom()
    {
        return GetComponentInParent<Room>();
    }

    /* Переключаем направление гейта
     * в зависимости от того, как бдует расположена комната
    */
    private void SwitchDirection(int offset)
    {
        int dir = (int)direction;
        dir += offset;
        dir %= 4;
        if (dir < 0) dir += 4;
        direction = (Direction)dir;
    }
}
