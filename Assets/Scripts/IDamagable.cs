﻿/// <summary>
/// Интерфейс для объектов, которые могут наносить урон
/// </summary>
public interface IDamagable {

    void DoDamage();
}
