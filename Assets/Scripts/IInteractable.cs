﻿/// <summary>
/// Интерфейс для объектов, с которыми можно взаимодействовать
/// </summary>
public interface IInteractable {

    void Interact(int damage);
}
