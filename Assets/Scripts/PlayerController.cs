﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// События для изменения уровня жизни игрока
/// </summary>
public partial class PlayerController
{
    public delegate void StartEventHandler(int maxHealth, int health);
    public delegate void ChangeHealthEventHandler(int health);
    public delegate void DeathEventHandler();
    public static event StartEventHandler StartEvent;
    public static event ChangeHealthEventHandler ChangeHealthEvent;
    public static event DeathEventHandler DeathEvent;
}

/// <summary>
/// Контроллер игрока
/// </summary>
public partial class PlayerController : MonoBehaviour, IDamagable, IInteractable
{
    [Tooltip("Радиус атаки")]
    [SerializeField]
    private float attackRadius;
    [Tooltip("Первоначальна задержка атаки")]
    [SerializeField]
    private float attackCooldown;
    [Tooltip("Пауза между атаками")]
    [SerializeField]
    private float attackRate = 1f;
    [Tooltip("Сила атаки (наносимый урон)")]
    [SerializeField]
    private int damageForce = 1;
    [Tooltip("Максимальное количество здоровья")]
    [SerializeField]
    private int maxHealth = 10;

    private int curHealth;
    private NavMeshAgent navMeshAgent;
    private Animator anim;
    private Transform enemyTransform;
    private bool enemyClicked;
    private bool _walking;
    private bool gameOver;

    void Start ()
    {
        Camera.main.GetComponent<CameraFollow>().SetTarget(gameObject);
        anim = GetComponentInChildren<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        curHealth = maxHealth;
        gameOver = false;
        if (StartEvent != null)
        {
            StartEvent(maxHealth, curHealth);
        }
    }
    
    // Update is called once per frame
    void Update () {

        
        if (gameOver) return;
        anim.SetBool("isWalking", _walking);
        anim.SetBool("isAttacking", false);

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Input.GetButtonDown("Fire1"))
        {
            navMeshAgent.ResetPath();
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (hit.collider.tag == "Enemy")
                {
                    enemyTransform = hit.transform;
                    enemyClicked = true;
                }
                else
                {
                    _walking = false;
                    enemyClicked = false;
                    navMeshAgent.isStopped = true;
                    
                    if (Time.time > attackCooldown)
                    {
                        transform.LookAt(hit.point);
                        Vector3 rayPoint = new Vector3(transform.position.x, transform.position.y + transform.localScale.y / 2, transform.position.z);
                        Vector3 forward = transform.TransformDirection(Vector3.forward) * attackRadius;
                        //Debug.DrawRay(rayPoint, forward, Color.green, 2);
                        Ray stayAttackRay = new Ray(rayPoint, forward);
                        RaycastHit stayHit;
                        if(Physics.Raycast(stayAttackRay, out stayHit))
                        {
                            if (stayHit.collider.tag == "Enemy")
                            {
                                enemyTransform = stayHit.transform;
                            }
                        }

                        attackCooldown = Time.time + attackRate;
                        anim.SetBool("isAttacking", true);
                    }
                }

            }

        }

        if (Input.GetButtonDown("Fire2"))
        {
            navMeshAgent.ResetPath();
            if(Physics.Raycast(ray, out hit, 1000))
            {
                if (hit.collider.tag != "Enemy")
                {
                    _walking = true;
                    enemyClicked = false;
                    navMeshAgent.isStopped = false;
                    navMeshAgent.SetDestination(hit.point);
                }

            }

        }

        if (enemyClicked)
        {
            MoveToAttack();
        }
        else
        {
            if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                _walking = false;
            }
            else if(( navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance))
            {
                _walking = true;
            }
        }
        
        anim.SetBool("isWalking", _walking);
        
    }

    /// <summary>
    /// Наносим цели урон
    /// </summary>
    public void DoDamage()
    {
        if (enemyTransform == null) return;
        enemyTransform.GetComponent<IInteractable>().Interact(damageForce);
    }

    /// <summary>
    /// Изменение уровня жизни игрока
    /// </summary>
    /// <param name="damage"></param>
    public void Interact(int damage)
    {
        curHealth -= damage;
        
        if (curHealth <= 0)
        {
            curHealth = 0;
            GameOver();
        }

        if (ChangeHealthEvent != null)
        {
            ChangeHealthEvent(curHealth);
        }
    }

    // обрабатываем "смерть" игрока
    void GameOver()
    {
        gameOver = true;
        anim.SetBool("isAttacking", false);
        anim.SetBool("isWalking", false);
        if (DeathEvent != null)
        {
            DeathEvent();
        }
    }

    // перемещаемся к врагу с последующей атакой
    void MoveToAttack()
    {
        if (enemyTransform == null) return;

        navMeshAgent.destination = enemyTransform.position;

        if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance >= attackRadius)
        {
            navMeshAgent.isStopped = false;
            _walking = true;
        }
        else if(!navMeshAgent.pathPending && navMeshAgent.remainingDistance <= attackRadius)
        {
            anim.SetBool("isAttacking", false);
            transform.LookAt(enemyTransform);

            if(Time.time > attackCooldown)
            {
                attackCooldown = Time.time + attackRate;
                anim.SetBool("isAttacking", true);
            }
            navMeshAgent.isStopped = true;
            _walking = false;
        }
    }

}
