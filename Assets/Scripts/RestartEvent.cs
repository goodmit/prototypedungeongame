﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartEvent : MonoBehaviour {

    /// <summary>
    /// Перезапуск уровня
    /// </summary>
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
