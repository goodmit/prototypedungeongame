﻿using UnityEngine;

public class Room : MonoBehaviour {

    public static int ROOM_STEP = 10;

    public Gate[] gates;
    [SerializeField]
    private GameObject enemySpawnList;
    public string type;

    public int[] rotIndexes { get; private set; }

    public bool playerIsHere { get; private set; }

    // Use this for initialization
    void Start() {

        int enemyCount = UnityEngine.Random.Range(1, 3);
        for (int i = 0; i < enemyCount; i++)
        {
            GameObject spawner = enemySpawnList.transform.GetChild(UnityEngine.Random.Range(0, enemySpawnList.transform.childCount)).gameObject;
            spawner.GetComponent<EnemySpawner>().SpawnEnemy();
            DestroyImmediate(spawner);
        }
        DestroyImmediate(enemySpawnList);
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (playerIsHere) return;
        if(other.tag == "Player")
        {
            print("игрок входит в комнату " + type);
            playerIsHere = true;
            GameController.Instance.UpdateRoomList(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!playerIsHere) return;
        if (other.tag == "Player")
        {
            print("игрок выходит из комнаты " + type);
            playerIsHere = false;
            
        }
    }

    /// <summary>
    /// В этой ли комнате сейчас игрок
    /// </summary>
    /// <returns></returns>
    public bool IsPlayerHere()
    {
        return playerIsHere;
    }

    /// <summary>
    /// Имеет ли комната свободные выходы
    /// (не соединенные с другими комнатами)
    /// </summary>
    /// <returns></returns>
    public bool IsCollapsed()
    {
        foreach (Gate gate in gates)
        {
            if (!gate.IsConnected()) return true;
        }
        return false;
    }

    /// <summary>
    /// Количество свободных гейтов в комнате
    /// </summary>
    /// <returns></returns>
    public int CollapseCount()
    {
        int result = 0;
        foreach (Gate gate in gates)
        {
            if (!gate.IsConnected()) result++;
        }
        return result;
    }

    /// <summary>
    /// Получаем индекс первого свободного гейта
    /// </summary>
    /// <returns></returns>
    public int GetFirstCollapsedIndex()
    {
        for (int i = 0; i < gates.Length; i++)
        {
            if (!gates[i].IsConnected())
            {
                return i;
            }
        }
        return -1;
    }

    /// <summary>
    /// Получаем направление первого свободного гейта
    /// </summary>
    /// <returns></returns>
    public Gate.Direction GetFirstFreeGateDirection()
    {
        for (int i = 0; i < gates.Length; i++)
        {
            if (!gates[i].IsConnected())
            {
                return gates[i].GetDirection();
            }
        }
        return Gate.Direction.NONE;
    }

    /// <summary>
    /// Находится ли комната в указанных координатах
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public bool IsPlacedInPos(Vector3 pos)
    {
        return transform.position == pos;
    }

    /// <summary>
    /// Обновляем состояние гейтов
    /// </summary>
    public void UpdateGates()
    {
        foreach (Gate gate in gates)
        {
            gate.UpdateState();
        }
    }

    /// <summary>
    /// Есть ли гейт в указанной стене комнаты
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    public bool HasGate(Gate.Direction dir)
    {
        foreach (Gate gate in gates)
        {
            if (gate.GetDirection() == dir)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Связываем гейт с другой комнатой
    /// </summary>
    /// <param name="room"></param>
    /// <param name="dir"></param>
    public void ConnectGate(Room room, Gate.Direction dir)
    {
        foreach (Gate gate in gates)
        {
            if(gate.GetDirection() == dir)
            {
                gate.SetLinkedRoom(room);
            }
        }
    }
    
    /// <summary>
    /// Очищаем возможные повороты для данного шаблона комнаты
    /// </summary>
    public void ClearRotations()
    {
        rotIndexes = null;
    }

    /// <summary>
    /// Записываем корректные повороты для данного шаблона комнаты
    /// </summary>
    /// <param name="indexes"></param>
    public void SetCorrectRotations(int[] indexes)
    {
        rotIndexes = indexes;
    }

}
