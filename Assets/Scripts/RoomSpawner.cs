﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class RoomSpawner : MonoBehaviour
{ 
    [Tooltip("Доступные шаблоны комнат для построения лабиринта")]
    [SerializeField]
    private Room[] roomPrefabs;
    [Tooltip("Стартовая комната")]
    [SerializeField]
    private Room startRoom;

    Room activeRoom;
    Room roomWithPlayer;

    private List<Room> spawnedRooms = new List<Room>();

    private void Start()
    {
        activeRoom = startRoom;
        spawnedRooms.Add(activeRoom);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            CreateRooms();
            StartCoroutine(CreateRooms());
        }
    }

    /// <summary>
    /// Получаем все свободные гейты на уровне
    /// </summary>
    /// <returns></returns>
    public List<Gate> CollapsedGates()
    {
        List<Gate> gates = new List<Gate>();
        foreach (Room room in spawnedRooms)
        {
            foreach (Gate gate in room.gates)
            {
                if (!gate.IsConnected())
                {
                    gates.Add(gate);
                }
            }
        }
        return gates;
    }

    /// <summary>
    /// Обновляем состояние комнат в лабиринте
    /// </summary>
    /// <param name="playerRoom"></param>
    /// <returns></returns>
    public IEnumerator UpdateRooms(Room playerRoom)
    {
        StopAllCoroutines();
        //roomWithPlayer = GetRoomWithPlayer();
        roomWithPlayer = playerRoom;
        List<Room> deniedRooms = new List<Room>();
        foreach (Room room in spawnedRooms)
        {
            if (!IsCorrectPosition(room.transform.position))
            {
                deniedRooms.Add(room);
            }
        }

        foreach (Room room in deniedRooms)
        {
            spawnedRooms.Remove(room);
            Destroy(room.gameObject);
        }
        deniedRooms.Clear();

        yield return new WaitForSeconds(0.1f);

        foreach (Room room in spawnedRooms)
        {
            room.UpdateGates();
        }

        StartCoroutine(CreateRooms());
    }

    // создание новых комнат
    private IEnumerator CreateRooms()
    {
        bool roomCreated = false;
        //roomWithPlayer = GetRoomWithPlayer();
        List<Gate> freeGates = CollapsedGates();

        if (freeGates.Count == 0)
        {
            Debug.LogWarning("Что-то пошло не так! Нет разрешенных мест для спавна новой комнаты либо все комнаты уже имеют соседей!");
            StopCoroutine(CreateRooms());
        }

        foreach (Gate gate in freeGates)
        {
            bool haveProblem = false;
            activeRoom = gate.GetParenRoom();
            if (activeRoom == null)
            {
                haveProblem = true;
                Debug.LogWarning("Что-то пошло не так! " + gate.name + "-" + gate.GetDirection().ToString() + " не привязан к комнате!");
                StopCoroutine(CreateRooms());
            }
            Room newRoom;

            Gate.Direction dir = gate.GetDirection();
            Vector3 newPos = activeRoom.transform.position + SetNewRoomPosition(dir);
            if (!IsCorrectPosition(newPos))
            {
                continue;
            }

            foreach (Room placedRoom in spawnedRooms)
            {
                if (placedRoom.IsPlacedInPos(newPos))
                {
                    haveProblem = true;
                    break;
                }
            }
            if (haveProblem) continue;

            // отбираем доступные шаблоны комнат
            Room prefabForNewRoom = GetProperRoom(CheckPrefabs(newPos));
            int rotationIndex = Random.Range(0, prefabForNewRoom.rotIndexes.Length);
            newRoom = Instantiate(prefabForNewRoom, newPos, Quaternion.Euler(0, prefabForNewRoom.rotIndexes[rotationIndex] * 90, 0), activeRoom.transform.parent);
            
            roomCreated = true;

            gate.SetLinkedRoom(newRoom);

            newRoom.ConnectGate(activeRoom, SpinDirection((int)dir));
            
            spawnedRooms.Add(newRoom);
            
        }
        if (roomCreated) StartCoroutine(CreateRooms());
        yield return null;
    }

    // ищем и добавляем в список все доступные комнаты для продолжения лабиринта
    private List<Room> GetAllowedRooms()
    {
        List<Room> rooms = new List<Room>();

        foreach (Room room in spawnedRooms)
        {
            if(IsCorrectPosition(room.transform.position) && room.IsCollapsed()) {
                rooms.Add(room);
            }
        }

        return rooms;
    }

    // получаем комнату где сейчас находится игрок
    private Room GetRoomWithPlayer()
    {
        foreach (Room room in spawnedRooms)
        {
            if (room.playerIsHere) return room;
        }
        
        StopCoroutine(CreateRooms());
        return null;
    }

    // меняем направление для прохода относительно комнаты
    private Gate.Direction SpinDirection(int baseRot, int offset = 2)
    {
        int result = baseRot + offset;
        result %= 4;
        if (result < 0) result += 4;
        return (Gate.Direction)result;
    }
    
    // проверяем, корректные ли координаты дял новой комнаты
    private bool IsCorrectPosition(Vector3 newPos)
    {
        return Mathf.Abs(Mathf.Round(newPos.x) - Mathf.Round(roomWithPlayer.transform.position.x)) <= Room.ROOM_STEP &&
               Mathf.Abs(Mathf.Round(newPos.z) - Mathf.Round(roomWithPlayer.transform.position.z)) <= Room.ROOM_STEP;
    }

    // получаем координаты для новой комнаты
    private Vector3 SetNewRoomPosition(Gate.Direction dir)
    {
        switch (dir)
        {
            case Gate.Direction.FORWARD:
                return new Vector3(0, 0, Room.ROOM_STEP);
            case Gate.Direction.RIGHT:
                return new Vector3(Room.ROOM_STEP, 0, 0);
            case Gate.Direction.BACK:
                return new Vector3(0, 0, -Room.ROOM_STEP);
            case Gate.Direction.LEFT:
                return new Vector3(-Room.ROOM_STEP, 0, 0);
            default:
                return Vector3.zero;
        }
    }
    
    // получаем подходящий шаблон комнаты
    private Room GetProperRoom(List<Room> list)
    {
        int index = Random.Range(0, list.Count);
        try
        {
            return list[index];
        }
        catch (System.Exception)
        {
            return list[index-1];
        }
    }
    
    // смещаем строку посимвольно
    private void ShiftString(ref string type)
    {
        // смещаем все символы в строке вправо на 1
        type = type.Substring(type.Length - 1) + type.Substring(0, 3);
    }

    // получаем список разрешенных для конкретной позиции шаблонов комнат
    private List<Room> CheckPrefabs(Vector3 newPos)
    {
        string pattern = GetPatternRoom(newPos);
        List<Room> allowedPrefabs = new List<Room>();
        foreach (Room room in roomPrefabs)
        {
            room.ClearRotations();
            string type = room.type;
            int[] rotationIndexes = MatchPattern(type, pattern);
            
            if (rotationIndexes.Length > 0)
            {
                // если открыт только один проход, то запрещаем выпадение тупиковой комнаты
                if(room.gates.Length == 1 && CollapsedGates().Count == 1)
                {
                    continue;
                }
                room.SetCorrectRotations(rotationIndexes);
                allowedPrefabs.Add(room);
            }
        }
        return allowedPrefabs;
    }

    /* проверяем по шаблону. возвращаем все совпавшие варианты 
     * в виде int[]. в массиве хранятся индексы углов разворота комнаты
     * если массив пуст, то комната не подходит.
     */
    private int[] MatchPattern(string type, string pattern)
    {
        List<int> rotIndexes = new List<int>();
        for (int i = 0; i < 4; i++)
        {
            if (Regex.IsMatch(type, pattern))
            {
                rotIndexes.Add(i);
            }
            ShiftString(ref type);
        }
        
        return rotIndexes.ToArray();
    }

    /* создаем шаблон для сверки типов возможных комнат которые
     * можно поместить в этих координатах*/
    private string GetPatternRoom(Vector3 position)
    {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < 4; i++)
        {
            Vector3 newPos = position + SetNewRoomPosition((Gate.Direction)i);
            bool keyAdded = false;
            foreach (Room placedRoom in spawnedRooms)
            {
                if (placedRoom.IsPlacedInPos(newPos))
                {
                    int value = (i + 2) % 4;
                    int key = placedRoom.HasGate( (Gate.Direction)(value) ) ? 1 : 0;
                    result.Append(key);
                    keyAdded = true;
                    break;
                }
            }
            if(!keyAdded) result.Append("\\d");
        }
        return result.ToString();
    }
}
