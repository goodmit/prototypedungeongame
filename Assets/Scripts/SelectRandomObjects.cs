﻿using UnityEngine;

public class SelectRandomObjects : MonoBehaviour {

    [Tooltip("Количество предметов, которые не будут удалены при запуске")]
    public int countToSave;

    private	void Start ()
    {
        while(transform.childCount > countToSave)
        {
            Transform child = transform.GetChild(Random.Range(0, transform.childCount));
            DestroyImmediate(child.gameObject);
        }
	}
	
	
}
