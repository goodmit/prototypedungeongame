﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Управляет UI элементами
/// </summary>
public class UIController : Singleton<UIController> {

    [Tooltip("Панель при проигрыше")]
    [SerializeField]
    private GameObject _gameOverPanel;
    [Tooltip("UI в игре")]
    [SerializeField]
    private GameObject _inGamePanel;
    [SerializeField]
    private Slider playerHealth;

    void OnEnable()
    {
        PlayerController.DeathEvent += OnGameOverEventHandler;
        PlayerController.StartEvent += OnStartEventHandler;
        PlayerController.ChangeHealthEvent += OnChangeHealthEventHandler;
    }

    void OnDisable()
    {
        PlayerController.DeathEvent -= OnGameOverEventHandler;
        PlayerController.StartEvent -= OnStartEventHandler;
        PlayerController.ChangeHealthEvent -= OnChangeHealthEventHandler;
    }

    // Use this for initialization
    void Start () {
        _gameOverPanel.SetActive(false);
        _inGamePanel.SetActive(true);
    }
	
    #region Listeners
    private void OnStartEventHandler(int maxHealthValue, int healthValue)
    {
        playerHealth.maxValue = maxHealthValue;
        playerHealth.value = healthValue;
    }

    private void OnChangeHealthEventHandler(int healthValue)
    {
        playerHealth.value = healthValue;
    }

    private void OnGameOverEventHandler()
    {
        _gameOverPanel.SetActive(true);
        _inGamePanel.SetActive(false);
    }
    #endregion
}
